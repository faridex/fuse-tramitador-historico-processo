package com.redhat.integration.processor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.redhat.integration.model.ProcessPublicHistory;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class HistoricoProcessoProcessor implements Processor {

    public void process(Exchange exchange) throws Exception {
        System.out.println("###arrive here ");
        String codigoProcesso = (String)exchange.getIn().getHeader("codigo-processo");
        //String jsonString = exchange.getIn(). getBody(String.class);
        String jsonString = callenpoint(codigoProcesso);
        System.out.println(jsonString);
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        List<ProcessPublicHistory> langList = mapper.readValue(jsonString, new TypeReference<List<ProcessPublicHistory>>(){});
        System.out.println("###return processo");
        exchange.getOut().setBody(langList);
        
    }

    private String callenpoint(String codigoProcesso){
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("https://kg9n0b08id.execute-api.sa-east-1.amazonaws.com/qa/process/public_history/"+codigoProcesso+"?bridgeEndpoint=true");
        httpGet.addHeader("content-type", "application/json");
        httpGet.addHeader("Authorization", "Token 9cd05770a2a1c174bf877754309e300b80cd8ff7");
        try{
            CloseableHttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String result = EntityUtils.toString(entity);
                System.out.println(result);
                return result;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
        /*
        BufferedReader br = new BufferedReader(
        		 new InputStreamReader((response.getEntity().getContent())));
        
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = br.readLine()) != null) {
        	result.append(line);
        }
        System.out.println("Response : \n"+result.append(line));
        return result.toString();
*/

    }
 
}