package com.redhat.integration.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties({ "permanecy"})
public class ProcessPublicHistory {
    private String status;
    private Capacity capacity;
    private String date;
    private String permanecy;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public Capacity getCapacity() {
        return capacity;
    }
    public void setCapacity(Capacity capacity) {
        this.capacity = capacity;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    @JsonIgnoreProperties
    public String getPermanecy() {
        return permanecy;
    }
    public void setPermanecy(String permanecy) {
        this.permanecy = permanecy;
    }



}
