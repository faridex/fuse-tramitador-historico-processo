package com.redhat.integration.model;

import java.util.List;

public class Capacity {
    private Integer id;
    private String description;
    private String abbreviation;
    private Integer entityId;
    private CapacityEntity entity;
    private List<CapacityAddress> address;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }
    public Integer getEntityId() {
        return entityId;
    }
    public void setEntityId(Integer entityId) {
        this.entityId = entityId;
    }
    public CapacityEntity getEntity() {
        return entity;
    }
    public void setEntity(CapacityEntity entity) {
        this.entity = entity;
    }
    public List<CapacityAddress> getAddress() {
        return address;
    }
    public void setAddress(List<CapacityAddress> address) {
        this.address = address;
    }

}
