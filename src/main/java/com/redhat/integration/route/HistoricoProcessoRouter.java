package com.redhat.integration.route;


import com.redhat.integration.model.ProcessPublicHistory;
import com.redhat.integration.processor.HistoricoProcessoProcessor;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

/**
 * A simple Camel REST DSL route that implements the greetings service.
 * 
 */
@Component
public class HistoricoProcessoRouter extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        // @formatter:off
        restConfiguration()
                .apiContextPath("/api-doc")
                .apiProperty("api.title", "Historico Processo API")
                .apiProperty("api.version", "1.0")
                .apiProperty("cors", "true")
                .apiProperty("base.path", "camel/")
                .apiProperty("api.path", "/")
                .apiProperty("host", "")
                .apiContextRouteId("doc-api")
            .component("servlet")
            .bindingMode(RestBindingMode.json);
        
        rest("/historico-processo").description("retorna todas as movimentações do processo de codigo {codigo-processo}")
            .get("/{codigo-processo}").outType(ProcessPublicHistory[].class)
            .route().routeId("historico-processo-api")
            .process(new HistoricoProcessoProcessor())
            .endRest();
            //.to("direct:callHistoricoProcessoService");

        from("direct:callHistoricoProcessoService").description("Greetings REST service implementation route")
        .setHeader(Exchange.HTTP_METHOD, simple("GET"))
        .setHeader("Authorization", simple("Token 9cd05770a2a1c174bf877754309e300b80cd8ff7"))
        .to("https://kg9n0b08id.execute-api.sa-east-1.amazonaws.com/qa/process/public_history/de4656aa-d230-450e-a924-b820124b6d55?bridgeEndpoint=true")
        .process(new HistoricoProcessoProcessor())
        .endRest();
            //.streamCaching()
            //.to("bean:greetingsService?method=getGreetings");     
        // @formatter:on
    }

}